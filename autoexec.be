# What was here is irrelevant now, nevermind

import math

# Taken from the spec sheet
class InvensensePressureConversion
  # Class for conversion of the pressure and temperature output of the Invensense sensor
  var p_Pa_calib
  var LUT_lower
  var LUT_upper
  var quadr_factor
  var offst_factor
  var A
  var B
  var C
  var sensor_constants

  def init(sensor_constants)
    # Initialize customer formula
    # Arguments:
    # sensor_constants -- list of 4 integers: [c1, c2, c3, c4]

    self.sensor_constants = sensor_constants
    # configuration for ICP-10125 Samples
    self.p_Pa_calib = [45000.0, 80000.0, 105000.0]
    self.LUT_lower = 3.5 * math.pow(2,20)
    self.LUT_upper = 11.5 * math.pow(2,20)
    self.quadr_factor = 1 / 16777216.0
    self.offst_factor = 2048.0
  end


  def calculate_conversion_constants(p_Pa, p_LUT)
    # calculate temperature dependent constants
    # Arguments:
    # p_Pa -- List of 3 values corresponding to applied pressure in Pa
    # p_LUT -- List of 3 values corresponding to the measured p_LUT values at the applied pressures.

    self.C = (p_LUT[0] * p_LUT[1] * (p_Pa[0] - p_Pa[1]) +
    p_LUT[1] * p_LUT[2] * (p_Pa[1] - p_Pa[2]) +
    p_LUT[2] * p_LUT[0] * (p_Pa[2] - p_Pa[0])) /
    (p_LUT[2] * (p_Pa[0] - p_Pa[1]) +
    p_LUT[0] * (p_Pa[1] - p_Pa[2]) +
    p_LUT[1] * (p_Pa[2] - p_Pa[0]))
    self.A = (p_Pa[0] * p_LUT[0] - p_Pa[1] * p_LUT[1] - (p_Pa[1] - p_Pa[0]) * self.C) / (p_LUT[0] - p_LUT[1])
    self.B = (p_Pa[0] - self.A) * (p_LUT[0] + self.C)
  end


  def get_pressure(p_LSB, T_LSB)
    # Convert an output from a calibrated sensor to a pressure in Pa.
    # Arguments:
    # p_LSB -- Raw pressure data from sensor
    # T_LSB -- Raw temperature data from sensor

    var t = T_LSB - 32768.0
    var s1 = self.LUT_lower + self.sensor_constants[0] * t * t * self.quadr_factor
    var s2 = self.offst_factor * self.sensor_constants[3] + self.sensor_constants[1] * t * t * self.quadr_factor
    var s3 = self.LUT_upper + self.sensor_constants[2] * t * t * self.quadr_factor
    self.calculate_conversion_constants(self.p_Pa_calib, [s1, s2, s3])
    return self.A + self.B / (self.C + p_LSB)
  end
end



class ICP10125
  var wire
  var sensor_constants
  var conv
  var counter



  def PRIVATE_ReadOTP()
    self.wire._begin_transmission(0x63)
      self.wire._write(0xC7)
      self.wire._write(0xF7)
    self.wire._end_transmission()
    tasmota.delay(100)
    self.wire._request_from(0x63, 3)
    var o1 = self.wire._read() 
    var o2 = self.wire._read() 
    var c1 = self.wire._read() 
    print("raw otp data:", str(o1), str(o2), str(c1))
    return o1*256+o2
  end



  def init()
    self.wire = tasmota.wire_scan(0x63)
    self.counter = 0

    # Read out OTP values required for measurement calculations
    self.wire._begin_transmission(0x63)
    self.wire._write(0xC5)
    self.wire._write(0x95)
    self.wire._write(0x00)
    self.wire._write(0x66)
    self.wire._write(0x9C)
    self.wire._end_transmission()

    var OTP1 = self.PRIVATE_ReadOTP()
    var OTP2 = self.PRIVATE_ReadOTP()
    var OTP3 = self.PRIVATE_ReadOTP()
    var OTP4 = self.PRIVATE_ReadOTP()
    self.sensor_constants = [OTP1, OTP2, OTP3, OTP4]

    self.conv = InvensensePressureConversion(self.sensor_constants)
  end



  def measure()
    # Send measurement command
    self.wire._begin_transmission(0x63)
      self.wire._write(0x48)
      self.wire._write(0xA3)
    self.wire._end_transmission()

    # Read out measurement
    tasmota.delay(300)      # This should be enough but it might need to be changed
    self.wire._request_from(0x63, 9, false)
    var p1 = self.wire._read()
    var p2 = self.wire._read()
    var c1 = self.wire._read()
    var p3 = self.wire._read()
    var p4 = self.wire._read()
    var c2 = self.wire._read()
    var t1 = self.wire._read()
    var t2 = self.wire._read()
    var c3 = self.wire._read()

    var pressure = self.conv.get_pressure(p1*256*256+p2*256+p3, t1*256+t2)
    print("Pressure:", pressure)
    tasmota.web_send_decimal(string.format("{s}ICP10125 Pressure: %i}", pressure))
    tasmota.response_append(string.format(",\"ICP10125\":{\"PRESSURE\":%i}", pressure))
  end

  def every_second()
    self.counter = self.counter + 1
    if self.counter == icp10125_measurement_delay
      self.counter = 0
      self.measure()
    end
  end
end


icp = ICP10125()
tasmota.add_driver(icp)
