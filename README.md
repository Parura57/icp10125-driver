# Invensense ICP-10125 Barometer

This is a driver for the Invensense ICP-10125 temperature and pressure sensor written in Berry specifically for use with the Tasmota firmware

## Configuration

### Wiring
| AHT   | ESP |
|---|---|
|VCC   |3.3V
|GND   |GND   
|SDA   | GPIOx
|SCL   | GPIOy

### Tasmota Settings 
In the **_Configuration -> Configure Module_** page assign:

1. GPIOx to `I2C SDA`
2. GPIOy to `I2C SCL`

After a reboot the driver should detect the sensor and display its readings in the webui, updating them once a minute (that can be changed in the code).

### Troubleshooting

Both the code and documentation was written pretty hastily so you might run into issues, although they should be fixable
without too much trouble from going through the code and Berry console. If that does happen feel free to message me and/or contribute
a pull request when it is fixed.

